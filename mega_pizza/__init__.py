import os

from flask import Flask
from flask import render_template
from flask import url_for
from flask import request
from flask import jsonify
from flask import abort
from flask import send_from_directory
from flask import redirect
from flask import url_for


APP = Flask(__name__)

APP.config.from_mapping(
    {
        "JSON_AS_ASCII": False
    }
)

@APP.route("/")
def index_page():
    return render_template("index.html")

@APP.route("/hello")
def hello_world():
    return "Hello, 🍕!"

@APP.route("/hello_html")
def hello_world_html():
    return "<h1>Hello, 🍕!</h1><hr />"

# @APP.route("/static")
# def static(file: str):
#     if not os.path.exists(os.path.join("static", file)):
#         abort(404)
#
#     return send_from_directory("static", file)

def get_pizza_menu():
    return [
        {
            "name": "Pizza Salami",
            "ingredients": [
                "Tomatensauce",
                "Käse",
                "Salami"
            ],
            "available": True
        },
        {
            "name": "Pizza Hawaii",
            "ingredients": [
                "Tomatensauce",
                "Käse",
                "Schinken",
                "Ananas"
            ],
            "available": True
        },
        {
            "name": "Pizza Caprese",
            "ingredients": [
                "Tomatensauce",
                "Tomatenscheiben",
                "Mozzarella",
                "Basilikum"
            ],
            "available": False
        },
        {
            "name": "Pizza Chicken Curry",
            "ingredients": [
                "Curry",
                "Hähnchenstücke",
                "Ananas"
            ],
            "available": False
        },
    ]

@APP.route("/contact")
def contact_page():
    return render_template(
        "contact.html",
        owner="P. Izza",
        phone_number="+49 421 1234567",
        place="Bremen"
    )

@APP.route("/menu")
def pizza_menu():
    return render_template(
        "menu.html",
        heading="Mega Pizza-Menü",
        pizzas=get_pizza_menu()
    )

@APP.route("/api/menu", methods=["GET"])
def pizza_menu_api():

    return jsonify(
        get_pizza_menu()
    )

@APP.route("/api/menu/query", methods=["GET"])
def pizza_menu_api_query():

    name = request.args.get("name")
    ingredient = request.args.get("ingredient")

    pizza_menu = get_pizza_menu()

    results = list()

    if not name and not ingredient:
        return redirect(url_for("pizza_menu_api"))

    for pizza in pizza_menu:
        if name == pizza.get("name"):
            results.append(pizza)
        elif ingredient in pizza.get("ingredients"):
            results.append(pizza)

    if not results:
        abort(404)

    return jsonify(results)
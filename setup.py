# -*- coding: utf-8 -*-

"""Setup file

Enables installation of this package and its dependencies

Author:
    Martin Schorfmann
Since:
    2019-08-16
Version:
    2019-08-16
"""

import os

from setuptools import find_packages, setup

# Package Meta Data
NAME = "megatalk-flask"
DESCRIPTION = "MegaTalk Flask"
URL = "https://gitlab.com/schorfma/megatalk-flask"
EMAIL = "martin.schorfmann@ohb-ds.de"
AUTHOR = "Martin Schorfmann"
REQUIRES_PYTHON = ">=3.7.0"
VERSION = "0.1.0"

# Required packages for execution
REQUIRED = [
    "Flask",     # flask
    "Jinja2",    # jinja2
    "Werkzeug",  # werkzeug
]

TESTS_REQUIRED = [

]

# Optional packages
EXTRAS = {}

# Root path of package
ROOT_PATH = os.path.abspath(
    os.path.dirname(__file__)
)

try:
    with open(os.path.join(ROOT_PATH, "README.md"), "r") as readme_file:
        LONG_DESCRIPTION = readme_file.read()
except FileNotFoundError:
    LONG_DESCRIPTION = DESCRIPTION

TEST_SUITE = "tests"

# Setup
setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(
        exclude=["tests"]
    ),
    install_requires=REQUIRED,
    tests_require=TESTS_REQUIRED,
    extras_require=EXTRAS,
    test_suite=TEST_SUITE
)

# MegaTalk Flask

> MegaTalk zum Thema "Web-Server mit _Flask_"

**Präsentation**: <https://schorfma.gitlab.io/megatalk-flask>

## Termin

_24.10.2019 16:00 Uhr_, OHB System Bremen Mehrzweckhalle

## Thema

> **Web-Server mit Flask**
>
> Leichtgewichtige Implementierung eines Web-Interfaces oder einer API mit dem _Python_ Microframework _Flask_
